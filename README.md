# APPLICATION TITLE: File Shredder

 **AUTHOR:**     Jim S. Smith

 **COPYRIGHT:**  (c)2023 - A FRESH WEB SOLUTION

 **LICENSE:**    GPL 2.0 or > (*with proper attribution please*)

 **VERSION:**    1.0.0e, *Updated:* 2023/12/24

 **USE:**        To SECURELY remove files from a file system.

 **DESCRIPTION:**

    A PHP-CLI script to securely destroy files before deleting them. This is
    done by overwriting the files, first with strings of random numbers. Then,
    the target files are overwritten with zeroes. Finally, they are deleted
    from the file system. All file-overwrites are rounded UP to the next
    "block size", to guarantee that the file contents will be completely
    overwritten.

 **REQUIREMENTS:**

 1. Should work in any PHP version,
 2. File-system functions, like 'fopen()' and 'fwrite()' must be usable,
 3. Ability to use PHP's "system()" function,
 4. The function 'random_bytes()' must be usable,

 **OTHER NOTES:**

 1. GREAT CARE should be taken when using this utility!

 2. Multiple file-paths can be entered as arguments.

 3. Of course, larger files will take longer to "shred" than smaller ones, but
    for the added security, this is still far better than just plain file-
    delete operations

 4. This script does not handle any wildcard characters in the file-paths.

 5. Any file-path argument which contains any spaces, or certain special meta-
    characters, will need to be enclosed in 's ( *single-quotes* ).

 6. This script can be copied to /usr/bin to make it available, server-wide.
   ( *Just make sure its permissions are set to: 0755.* )

 **WARNING:**

    Once a file is "destroyed" using this script, its contents may most likely
    NEVER be recoverable.
